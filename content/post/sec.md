---
title: "5月24日"
date: 2020-05-26T22:00:53-05:00
tags: [Japanese, text]
draft: false
---

# Lo que vimos

* まだ
* もう/ も

# どうし

* そうじします
* かたづけます

* ごうかくします
* しっぱいします

# Extra

* ばいかく - Unam, Tec.
* こうこう - Prepa
* ちゅがっじょう - Secundaria
* しょうがっこう - Primaria

## Estudiante de ...
* ばいがくせい
* こうこうせい
* ちゅうがくせい
* しょうがくせい

## Periodos Estudantil - がっき
Estan divididos por años.

* 1年生 - いち ねんせい
* 2年生 - に ねんせい
* 3年生 - さん ねんせい

